package postgres_client

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

type Config struct {
	Host string
	Port string
	Name string
	User string
	Pass string
}

func NewDBPool(cfg Config) (*sql.DB, error) {
	addr := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		cfg.User,
		cfg.Pass,
		cfg.Host,
		cfg.Port,
		cfg.Name,
	)

	pool, err := sql.Open("postgres", addr)
	if err != nil {
		return nil, err
	}

	return pool, nil
}
