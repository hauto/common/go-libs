package amqp_client

import (
	amqp "github.com/rabbitmq/amqp091-go"
)

type listener struct {
	connection *amqp.Connection
	channel    *amqp.Channel
}

func listen(addr string) (listener, error) {
	conn, err := amqp.Dial(addr)
	if err != nil {
		return listener{}, err
	}

	ch, err := conn.Channel()
	if err != nil {
		return listener{}, err
	}

	return listener{
		connection: conn,
		channel:    ch,
	}, nil
}
