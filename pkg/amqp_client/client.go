package amqp_client

import (
	"context"
	"fmt"
	"sync"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
)

type HandlerFunc func(*amqp.Channel, amqp.Delivery)

type MiddlewareFunc func(HandlerFunc) HandlerFunc

type Config struct {
	Host string
	Port string
	User string
	Pass string

	GracefulShutdownTimeout time.Duration
}

type Client struct {
	config Config

	handlers    map[string]HandlerFunc
	middlewares []MiddlewareFunc

	ctx        context.Context
	wg         *sync.WaitGroup
	inShutdown bool
}

func NewClient(ctx context.Context, cfg Config) *Client {
	return &Client{
		config: cfg,

		handlers:    make(map[string]HandlerFunc),
		middlewares: make([]MiddlewareFunc, 0),

		ctx: ctx,
		wg:  &sync.WaitGroup{},
	}
}

func (c *Client) AddHandler(queue string, handler HandlerFunc) {
	c.handlers[queue] = handler
}

func (c *Client) AddMiddleware(middleware MiddlewareFunc) {
	c.middlewares = append(c.middlewares, middleware)
}

func (c *Client) Shutdown() error {
	c.inShutdown = true

	ctx, cancel := context.WithTimeout(c.ctx, c.config.GracefulShutdownTimeout)
	defer cancel()

	done := make(chan struct{})
	go func() {
		c.wg.Wait()
		close(done)
	}()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-done:
		return nil
	}
}

func (c *Client) ListenAndServe() error {
	addr := fmt.Sprintf(
		"amqp://%s:%s@%s:%s",
		c.config.User,
		c.config.Pass,
		c.config.Host,
		c.config.Port,
	)

	ln, err := listen(addr)
	if err != nil {
		return err
	}

	return c.serve(ln)
}

func (c *Client) serve(ln listener) error {
	defer ln.connection.Close()
	defer ln.channel.Close()

	for queueName, handler := range c.handlers {
		c.wg.Add(1)

		queue, err := declareQueue(ln.channel, queueName)
		if err != nil {
			return err
		}

		msgChan, err := consume(ln.channel, queue)
		if err != nil {
			return err
		}

		go func(h HandlerFunc) {
			c.handle(ln.channel, h, msgChan)
			c.wg.Done()
		}(handler)
	}

	<-c.ctx.Done()

	return nil

}

func (c *Client) handle(ch *amqp.Channel, handler HandlerFunc, msgChan <-chan amqp.Delivery) {
	for {
		select {
		case <-c.ctx.Done():
			return
		case msg := <-msgChan:
			handler(ch, msg)
		default:
			if c.inShutdown {
				return
			}
		}
	}
}
