package amqp_client

import (
	amqp "github.com/rabbitmq/amqp091-go"
)

func declareQueue(ch *amqp.Channel, queueName string) (amqp.Queue, error) {
	return ch.QueueDeclare(queueName, false, true, false, false, nil)
}

func consume(ch *amqp.Channel, queue amqp.Queue) (<-chan amqp.Delivery, error) {
	return ch.Consume(queue.Name, "", true, false, false, false, nil)
}
