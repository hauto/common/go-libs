module gitlab.com/hauto/common/go-libs

go 1.21.1

require github.com/rabbitmq/amqp091-go v1.8.1

require github.com/lib/pq v1.10.9 // indirect
